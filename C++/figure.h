#ifndef FIGURE_H
#define FIGURE_H

#include <QtCore>
#include <QtQml>

class Figure : public QObject
{
    Q_OBJECT
public:
    enum class FigureType {
        General  = 0,
        Advisor  = 1,
        Cannon   = 2,
        Chariot  = 3,
        Elephant = 4,
        Horse    = 5,
        Soldier  = 6,
        None     = 7,
    };
    Q_ENUMS(FigureType)

    enum class PlayerSide {
        Red = 0,
        White = 1,
        None = 2,
        OutOfDesk = 3,
    };

private:
    Figure::FigureType figureType;
    Figure::PlayerSide side;
    std::pair <int, int> position;
    std::pair <int, int> defaultPosition;

public:
    Figure(FigureType figureType, PlayerSide side, std::pair <int, int> position = std::make_pair(INT_MAX, INT_MAX));
    Figure(std::pair <int, int> position = std::make_pair(INT_MAX, INT_MAX));
    Figure(const Figure &f);

    std::pair <int, int> getPosition() const;
    std::pair <int, int> getDefaultPosition() const;
    bool isOnDesk() const;
    PlayerSide getSide() const;
    FigureType getFigureType() const;

    void setPosition(std::pair <int, int> position);
    void setDefaultPosition();

    Q_INVOKABLE int getXPosition() { return position.first;  }
    Q_INVOKABLE int getYPosition() { return position.second; }
};

#endif // FIGURE_H
