#include "player.h"

Player::Player(PlayerControl control, QString playerName, Figure::PlayerSide side)
    : playerName(playerName), side(side), control(control)
{
    int i(0);
    if (side == Figure::PlayerSide::White)
        i = 9;

    figureList.clear();
    figureList.push_back(Figure(Figure::FigureType::Chariot,  side, std::make_pair(0, i)));
    figureList.push_back(Figure(Figure::FigureType::Horse,    side, std::make_pair(1, i)));
    figureList.push_back(Figure(Figure::FigureType::Elephant, side, std::make_pair(2, i)));
    figureList.push_back(Figure(Figure::FigureType::Advisor,  side, std::make_pair(3, i)));
    figureList.push_back(Figure(Figure::FigureType::General,  side, std::make_pair(4, i)));
    figureList.push_back(Figure(Figure::FigureType::Elephant, side, std::make_pair(6, i)));
    figureList.push_back(Figure(Figure::FigureType::Advisor,  side, std::make_pair(5, i)));
    figureList.push_back(Figure(Figure::FigureType::Horse,    side, std::make_pair(7, i)));
    figureList.push_back(Figure(Figure::FigureType::Chariot,  side, std::make_pair(8, i)));

    figureList.push_back(Figure(Figure::FigureType::Cannon,  side,  std::make_pair(1, abs(i-2))));
    figureList.push_back(Figure(Figure::FigureType::Cannon,  side,  std::make_pair(7, abs(i-2))));

    figureList.push_back(Figure(Figure::FigureType::Soldier, side,  std::make_pair(0, abs(i-3))));
    figureList.push_back(Figure(Figure::FigureType::Soldier, side,  std::make_pair(2, abs(i-3))));
    figureList.push_back(Figure(Figure::FigureType::Soldier, side,  std::make_pair(4, abs(i-3))));
    figureList.push_back(Figure(Figure::FigureType::Soldier, side,  std::make_pair(6, abs(i-3))));
    figureList.push_back(Figure(Figure::FigureType::Soldier, side,  std::make_pair(8, abs(i-3))));
}

std::pair <int, int> Player::getDefaultFigurePosition(Figure::FigureType type, int number)
{
    int currentNumber = -1;
    for (auto figure : figureList){
        if (figure.getFigureType() == type){
            currentNumber++;
            if (currentNumber == number)
                return figure.getDefaultPosition();
        }
    }
    return std::make_pair(INT_MAX, INT_MAX);
}

void Player::setDefaultFigure()
{
    for (auto f : figureList)
        f.setDefaultPosition();
}

Figure::FigureType Player::getFigureTypeOnPlace(int x, int y)
{
    if (x < 0 || x > 8 || y < 0 || y > 9)
        return Figure::FigureType::None;
    for (auto f : figureList)
        if (f.getXPosition() == x && f.getYPosition() == y)
            return f.getFigureType();
    return Figure::FigureType::None;
}

Figure Player::getFigureOnPlace(int x, int y)
{
    for (auto f : figureList)
        if (std::make_pair(x, y) == f.getPosition())
            return f;
    return Figure();
}

Figure Player::getFigure(Figure::FigureType type, int number)
{
    int currentNumber = -1;
    for (auto figure : figureList){
        if (figure.getFigureType() == type){
            currentNumber++;
            if (currentNumber == number)
                return figure;
        }
    }
    return Figure();
}

bool Player::setNewPositionToFugure(std::pair<int, int> oldCord, std::pair<int, int> newCord)
{
    for (int i = 0; i < figureList.length(); i++)
        if (figureList[i].getPosition() == oldCord){
            figureList[i].setPosition(newCord);
            return true;
        }
    return false;
}
