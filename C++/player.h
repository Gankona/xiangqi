#ifndef PLAYER_H
#define PLAYER_H

#include <QtCore>
#include "figure.h"
#include <math.h>

class Player : public QObject
{
    Q_OBJECT
public:
    enum class PlayerControl {
        AI = 0,
        localPlayer = 1,
        networkPlayer = 2,
    };

private:
    QString playerName;
    QList <Figure> figureList;
    Figure::PlayerSide side;
    Player::PlayerControl control;

public:
    Player(PlayerControl control, QString playerName = "Guest", Figure::PlayerSide side = Figure::PlayerSide::None);

    std::pair <int, int> getDefaultFigurePosition(Figure::FigureType type, int number);
    QList <Figure> getFigureList() const { return figureList; }
    Figure getFigure(Figure::FigureType type, int number = 0);
    void setDefaultFigure();
    Player::PlayerControl getControl() const { return control; }
    Figure::PlayerSide getSide() { return side; }
    Figure::FigureType getFigureTypeOnPlace(int x, int y);
    Figure getFigureOnPlace(int x, int y);
    bool setNewPositionToFugure(std::pair <int, int> oldCord, std::pair <int, int> newCord);

signals:
    void go();
};

#endif // PLAYER_H
