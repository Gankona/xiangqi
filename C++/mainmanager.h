#ifndef MAINMANAGER_H
#define MAINMANAGER_H

#include <QtCore>
#include <QtGui>
#include <QQuickView>
#include <QQmlContext>
#include <QQuickItem>

#include "figure.h"
#include "account.h"
#include "matchmanager.h"
#include "player.h"

class MainManager : public QObject
{
    Q_OBJECT
private:
    QQuickView *view;
    MatchManager *match;
    QObject *deskScene;
    Account *currentAccount;

public:
    explicit MainManager(QObject *parent = nullptr);

    Q_INVOKABLE void quit();
};

#endif // MAINMANAGER_H
