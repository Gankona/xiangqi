#ifndef ACCOUNT_H
#define ACCOUNT_H

#include <QtCore>

class Account : public QObject
{
    Q_OBJECT
private:
    QString login;
    int winGame;
    int loseGame;
    int drawGame;

public:
    explicit Account(QObject *parent = 0);

signals:

public slots:
};

#endif // ACCOUNT_H
