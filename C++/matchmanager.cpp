#include "matchmanager.h"

MatchManager::MatchManager()
{
    topPlayer = new Player(Player::PlayerControl::localPlayer, "", Figure::PlayerSide::Red);
    bottomPlayer = new Player(Player::PlayerControl::localPlayer, "", Figure::PlayerSide::White);
    currentPlayer = topPlayer;
}

void MatchManager::setDesk(QObject *desk)
{
    qDebug() << "setDesk 12-matchManager.cpp";
    this->desk = desk;
}

void MatchManager::setDefaultField()
{
    topPlayer->setDefaultFigure();
    bottomPlayer->setDefaultFigure();
}

void MatchManager::findPossibleTurn(int x, int y)
{
    Player *currentChoosePlayer;
    if (topPlayer->getControl() == Player::PlayerControl::AI
           || topPlayer->getControl() == Player::PlayerControl::networkPlayer
           || bottomPlayer->getControl() == Player::PlayerControl::AI
           || bottomPlayer->getControl() == Player::PlayerControl::networkPlayer){
        if (topPlayer->getControl() == Player::PlayerControl::localPlayer)
            currentChoosePlayer = topPlayer;
        else
            currentChoosePlayer = bottomPlayer;
    }
    else
        currentChoosePlayer = currentPlayer;

    bool isEnemy;
    Figure::PlayerSide color = currentChoosePlayer->getSide();

    if (currentPlayer->getSide() == getFigureSideByCoordinate(x, y)){
        isEnemy = false;
        color == Figure::PlayerSide::Red
                ? color = Figure::PlayerSide::White
                : color = Figure::PlayerSide::Red;
    }
    else if (getNotCurrentPlayer()->getSide() == getFigureSideByCoordinate(x, y))
        isEnemy = true;
    else {
        QMetaObject::invokeMethod(desk, "clearTurnLayer");
        return;
    }

    // player (enemy)
    int currentPosition(x * 10 + y);
    QList <int> firstList; // enemyFigureList(playerPathList)
    QList <int> secondList; // playerPathList(enemyPathList)

    switch (getFigureTypeByCoordinate(x, y)) {
    case Figure::FigureType::Advisor: {
        for (int i = 0; i < 10; i++)
            if (i <= 2 || i >= 7)
                for (int j = 0; j < 9; j++)
                    if (j >= 3 && j <= 5)
                        if ((abs(x - j) == 1 && abs(y - i) == 1)){
                            if (getFigureSideByCoordinate(j, i) == color) // enemyColor
                                firstList.push_back(j * 10 + i); // enemyFigureList
                            else if (getFigureSideByCoordinate(j, i) == Figure::PlayerSide::None)
                                secondList.push_back(j * 10 + i); // playerPathList
                        }
        break;
    }
    case Figure::FigureType::Cannon: {
        int tempX(x);
        int tempY(y);
        bool screenPresent(false);
        // right
        while (true){
            tempX++;
            if (getFigureSideByCoordinate(tempX, y) == Figure::PlayerSide::OutOfDesk)
                break;
            else if (getFigureSideByCoordinate(tempX, y) == Figure::PlayerSide::None && !screenPresent)
                secondList.push_back(tempX * 10 + y);
            else if (screenPresent){
                if (getFigureSideByCoordinate(tempX, y) == color){
                    firstList.push_back(tempX * 10 + y);
                    break;
                }
                else if (getFigureSideByCoordinate(tempX, y) != Figure::PlayerSide::None)
                    break;
            }
            else
                screenPresent = true;
        }
        tempX = x;
        screenPresent = false;
        // left
        while (true){
            tempX--;
            if (getFigureSideByCoordinate(tempX, y) == Figure::PlayerSide::OutOfDesk)
                break;
            else if (getFigureSideByCoordinate(tempX, y) == Figure::PlayerSide::None && !screenPresent)
                secondList.push_back(tempX * 10 + y);
            else if (screenPresent){
                if (getFigureSideByCoordinate(tempX, y) == color){
                    firstList.push_back(tempX * 10 + y);
                    break;
                }
                else if (getFigureSideByCoordinate(tempX, y) != Figure::PlayerSide::None)
                    break;
            }
            else
                screenPresent = true;
        }
        screenPresent = false;
        // up
        while (true){
            tempY--;
            if (getFigureSideByCoordinate(x, tempY) == Figure::PlayerSide::OutOfDesk)
                break;
            else if (getFigureSideByCoordinate(x, tempY) == Figure::PlayerSide::None && !screenPresent)
                secondList.push_back(x * 10 + tempY);
            else if (screenPresent){
                if (getFigureSideByCoordinate(x, tempY) == color){
                    firstList.push_back(x * 10 + tempY);
                    break;
                }
                else if (getFigureSideByCoordinate(x, tempY) != Figure::PlayerSide::None)
                    break;
            }
            else
                screenPresent = true;
        }
        tempY = y;
        screenPresent = false;
        // down
        while (true){
            tempY++;
            qDebug() << x << tempY << static_cast<int>(getFigureSideByCoordinate(x, tempY));
            if (getFigureSideByCoordinate(x, tempY) == Figure::PlayerSide::OutOfDesk)
                break;
            else if (getFigureSideByCoordinate(x, tempY) == Figure::PlayerSide::None && !screenPresent)
                secondList.push_back(x * 10 + tempY);
            else if (screenPresent){
                if (getFigureSideByCoordinate(x, tempY) == color){
                    firstList.push_back(x * 10 + tempY);
                    break;
                }
                else if (getFigureSideByCoordinate(x, tempY) != Figure::PlayerSide::None)
                    break;
            }
            else
                screenPresent = true;
        }
        break;
    }
    case Figure::FigureType::Chariot: {
        int tempX(x);
        int tempY(y);
        // up
        while (true){
            tempX++;
            if (getFigureSideByCoordinate(tempX, y) == color){
                firstList.push_back(tempX * 10 + y);
                break;
            }
            else if (getFigureSideByCoordinate(tempX, y) == Figure::PlayerSide::None)
                secondList.push_back(tempX * 10 + y);
            else
                break;
        }
        tempX = x;
        // down
        while (true){
            tempX--;
            if (getFigureSideByCoordinate(tempX, y) == color){
                firstList.push_back(tempX * 10 + y);
                break;
            }
            else if (getFigureSideByCoordinate(tempX, y) == Figure::PlayerSide::None)
                secondList.push_back(tempX * 10 + y);
            else
                break;
        }
        // left
        while (true){
            tempY--;
            if (getFigureSideByCoordinate(x, tempY) == color){
                firstList.push_back(x * 10 + tempY);
                break;
            }
            else if (getFigureSideByCoordinate(x, tempY) == Figure::PlayerSide::None)
                secondList.push_back(x * 10 + tempY);
            else
                break;
        }
        tempY = y;
        // right
        while (true){
            tempY++;
            if (getFigureSideByCoordinate(x, tempY) == color){
                firstList.push_back(x * 10 + tempY);
                break;
            }
            else if (getFigureSideByCoordinate(x, tempY) == Figure::PlayerSide::None)
                secondList.push_back(x * 10 + tempY);
            else
                break;
        }
        break;
    }
    case Figure::FigureType::Elephant: {
        bool isCanMoveUp  ((y <= 4 && y + 2 <= 4) || (y >= 5 && y + 2 <= 9));
        bool isCanMoveDown((y >= 5 && y - 2 >= 4) || (y <= 4 && y - 2 >= 0));
        bool isCanMoveLeft (x - 2 >= 0);
        bool isCanMoveRight(x + 2 <= 8);

        // up - left
        if (isCanMoveUp && isCanMoveLeft)
            if (getFigureSideByCoordinate(x - 1, y + 1) == Figure::PlayerSide::None){
                if (getFigureSideByCoordinate(x - 2, y + 2) == color)
                    firstList.push_back((x - 2) * 10 + y + 2);
                else if (getFigureSideByCoordinate(x - 2, y + 2) == Figure::PlayerSide::None)
                    secondList.push_back((x - 2) * 10 + y + 2);
            }
        // down - left
        if (isCanMoveDown && isCanMoveLeft)
            if (getFigureSideByCoordinate(x - 1, y - 1) == Figure::PlayerSide::None){
                if (getFigureSideByCoordinate(x - 2, y - 2) == color)
                    firstList.push_back((x - 2) * 10 + y - 2);
                else if (getFigureSideByCoordinate(x - 2, y - 2) == Figure::PlayerSide::None)
                    secondList.push_back((x - 2) * 10 + y - 2);
            }
        // down - right
        if (isCanMoveDown && isCanMoveRight)
            if (getFigureSideByCoordinate(x + 1, y - 1) == Figure::PlayerSide::None){
                if (getFigureSideByCoordinate(x + 2, y - 2) == color)
                    firstList.push_back((x + 2) * 10 + y - 2);
                else if (getFigureSideByCoordinate(x + 2, y - 2) == Figure::PlayerSide::None)
                    secondList.push_back((x + 2) * 10 + y - 2);
            }
        // up - right
        if (isCanMoveUp && isCanMoveRight)
            if (getFigureSideByCoordinate(x + 1, y + 1) == Figure::PlayerSide::None){
                if (getFigureSideByCoordinate(x + 2, y + 2) == color)
                    firstList.push_back((x + 2) * 10 + y + 2);
                else if (getFigureSideByCoordinate(x + 2, y + 2) == Figure::PlayerSide::None)
                    secondList.push_back((x + 2) * 10 + y + 2);
            }
        break;
    }
    case Figure::FigureType::General: {
        for (int i = 0; i < 10; i++)
            if (i <= 2 || i >= 7)
                for (int j = 0; j < 9; j++)
                    if (j >= 3 && j <= 5)
                        if ((abs(x - j) == 1 && y == i)
                                || (x == j && abs(y - i) == 1)){
                            if (getFigureSideByCoordinate(j, i) == color)
                                firstList.push_back(j * 10 + i);
                            else if (getFigureSideByCoordinate(j, i) == Figure::PlayerSide::None)
                                secondList.push_back(j * 10 + i);
                        }
        break;
    }
    case Figure::FigureType::Horse: {
        // up
        if (getFigureSideByCoordinate(x, y - 1) == Figure::PlayerSide::None){
            if (getFigureSideByCoordinate(x - 1, y - 2) == color)
                firstList.push_back((x - 1) * 10 + y - 2);
            else if (getFigureSideByCoordinate(x - 1, y - 2) == Figure::PlayerSide::None)
                secondList.push_back((x - 1) * 10 + y - 2);
            if (getFigureSideByCoordinate(x + 1, y - 2) == color)
                firstList.push_back((x + 1) * 10 + y - 2);
            else if (getFigureSideByCoordinate(x + 1, y - 2) == Figure::PlayerSide::None)
                secondList.push_back((x + 1) * 10 + y - 2);
        }
        // down
        if (getFigureSideByCoordinate(x, y + 1) == Figure::PlayerSide::None){
            if (getFigureSideByCoordinate(x - 1, y + 2) == color)
                firstList.push_back((x - 1) * 10 + y + 2);
            else if (getFigureSideByCoordinate(x - 1, y + 2) == Figure::PlayerSide::None)
                secondList.push_back((x - 1) * 10 + y + 2);
            if (getFigureSideByCoordinate(x + 1, y + 2) == color)
                firstList.push_back((x + 1) * 10 + y + 2);
            else if (getFigureSideByCoordinate(x + 1, y + 2) == Figure::PlayerSide::None)
                secondList.push_back((x + 1) * 10 + y + 2);
        }
        // left
        if (getFigureSideByCoordinate(x - 1, y) == Figure::PlayerSide::None){
            if (getFigureSideByCoordinate(x - 2, y - 1) == color)
                firstList.push_back((x - 2) * 10 + y - 1);
            else if (getFigureSideByCoordinate(x - 2, y - 1) == Figure::PlayerSide::None)
                secondList.push_back((x - 2) * 10 + y - 1);
            if (getFigureSideByCoordinate(x - 2, y + 1) == color)
                firstList.push_back((x - 2) * 10 + y + 1);
            else if (getFigureSideByCoordinate(x - 2, y + 1) == Figure::PlayerSide::None)
                secondList.push_back((x - 2) * 10 + y + 1);
        }
        // right
        if (getFigureSideByCoordinate(x + 1, y) == Figure::PlayerSide::None){
            if (getFigureSideByCoordinate(x + 2, y - 1) == color)
                firstList.push_back((x + 2) * 10 + y - 1);
            else if (getFigureSideByCoordinate(x + 2, y - 1) == Figure::PlayerSide::None)
                secondList.push_back((x + 2) * 10 + y - 1);
            if (getFigureSideByCoordinate(x + 2, y + 1) == color)
                firstList.push_back((x + 2) * 10 + y + 1);
            else if (getFigureSideByCoordinate(x + 2, y + 1) == Figure::PlayerSide::None)
                secondList.push_back((x + 2) * 10 + y + 1);
        }
        break;
    }
    case Figure::FigureType::Soldier: {
        int yShift(1);
        if (getFigureByCoordinate(x, y).getDefaultPosition().second
                == bottomPlayer->getFigure(Figure::FigureType::Soldier).getDefaultPosition().second)
            yShift = -1;

        // before river
        if (getFigureSideByCoordinate(x, y + yShift) == color)
            firstList.push_back(x * 10 + y + yShift);
        else if (getFigureSideByCoordinate(x, y + yShift) == Figure::PlayerSide::None)
            secondList.push_back(x * 10 + y + yShift);

        if ((y >= 5 && yShift == 1) || (y <= 4 && yShift == -1)){
            // after river
            // left
            qDebug() << "posible cor" << x - 1 << y << "\n" << x + 1 << y << "\n" << x << y + yShift;
            if (getFigureSideByCoordinate(x - 1, y) == color)
                firstList.push_back((x - 1) * 10 + y);
            else if (getFigureSideByCoordinate(x - 1, y) == Figure::PlayerSide::None)
                secondList.push_back((x - 1) * 10 + y);
            // right
            if (getFigureSideByCoordinate(x + 1, y) == color)
                firstList.push_back((x + 1) * 10 + y);
            else if (getFigureSideByCoordinate(x + 1, y) == Figure::PlayerSide::None)
                secondList.push_back((x + 1) * 10 + y);
        }

        break;
    }
    default:
        QMetaObject::invokeMethod(desk, "clearTurnLayer");
        return;
    }

    QList <int> enemyFigureList;
    if (isEnemy) {
        possibleTurnPlaceList.clear();
        cordByLastChooseFigure = std::make_pair(INT_MAX, INT_MAX);
        enemyFigureList << currentPosition;
        QMetaObject::invokeMethod(desk, "setPossibleTurn",
                                  Q_ARG(QVariant, currentPosition),
                                  Q_ARG(QVariant, QVariant::fromValue(QList<int>{currentPosition})),
                                  Q_ARG(QVariant, QVariant::fromValue(secondList)),
                                  Q_ARG(QVariant, QVariant::fromValue(firstList)));
    }
    else {
        possibleTurnPlaceList.clear();
        cordByLastChooseFigure = std::make_pair(x, y);
        for (auto i : firstList)
            possibleTurnPlaceList.push_back(std::make_pair (i/10, i%10));
        for (auto i : secondList)
            possibleTurnPlaceList.push_back(std::make_pair (i/10, i%10));
        QMetaObject::invokeMethod(desk, "setPossibleTurn",
                                  Q_ARG(QVariant, currentPosition),
                                  Q_ARG(QVariant, QVariant::fromValue(firstList)),
                                  Q_ARG(QVariant, QVariant::fromValue(QList <int>())),
                                  Q_ARG(QVariant, QVariant::fromValue(secondList)));
    }
}

Figure MatchManager::getFigureByCoordinate(int x, int y)
{
    if (getFigureSideByCoordinate(x, y) == topPlayer->getSide())
        return topPlayer->getFigureOnPlace(x, y);
    return bottomPlayer->getFigureOnPlace(x, y);
}

Figure::PlayerSide MatchManager::getFigureSideByCoordinate(int x, int y)
{
    if (x >= 0 && x <= 8 && y >= 0 && y <= 9){
        auto ret = topPlayer->getFigureTypeOnPlace(x, y);
        if (ret != Figure::FigureType::None)
            return topPlayer->getSide();
        ret = bottomPlayer->getFigureTypeOnPlace(x, y);
        if (ret != Figure::FigureType::None)
            return bottomPlayer->getSide();
        return Figure::PlayerSide::None;
    }
    return Figure::PlayerSide::OutOfDesk;
}

Figure::FigureType MatchManager::getFigureTypeByCoordinate(int x, int y)
{
    auto ret = topPlayer->getFigureTypeOnPlace(x, y);
    if (ret == Figure::FigureType::None)
        ret = bottomPlayer->getFigureTypeOnPlace(x, y);
    return ret;
}

QString MatchManager::getImagePath(QString color, int type)
{
    QString imagePath = "qrc:/figure/svg/";
    switch (static_cast<Figure::FigureType>(type)) {
    case Figure::FigureType::Advisor:  imagePath += "advisor";  break;
    case Figure::FigureType::Cannon:   imagePath += "cannon";   break;
    case Figure::FigureType::Chariot:  imagePath += "chariot";  break;
    case Figure::FigureType::Elephant: imagePath += "elephant"; break;
    case Figure::FigureType::General:  imagePath += "general";  break;
    case Figure::FigureType::Horse:    imagePath += "horse";    break;
    case Figure::FigureType::Soldier:  imagePath += "soldier";  break;
    default:;
    }
    color == "red" ? imagePath += "R" : imagePath += "W";
    imagePath += ".svg.png";
    return imagePath;
}

int MatchManager::getXFigurePosition(QString color, int type, int number)
{
    if (color.toLower() == "red" || color.at(0).toLower() == 'r'){
        if (topPlayer->getSide() == Figure::PlayerSide::Red)
            return topPlayer->getFigure(static_cast<Figure::FigureType>(type), number).getXPosition();
        else
            return bottomPlayer->getFigure(static_cast<Figure::FigureType>(type), number).getXPosition();
    }
    else if (topPlayer->getSide() == Figure::PlayerSide::White)
        return topPlayer->getFigure(static_cast<Figure::FigureType>(type), number).getXPosition();
    return bottomPlayer->getFigure(static_cast<Figure::FigureType>(type), number).getXPosition();
}

int MatchManager::getYFigurePosition(QString color, int type, int number)
{
    if (color.toLower() == "red" || color.at(0).toLower() == 'r'){
        if (topPlayer->getSide() == Figure::PlayerSide::Red)
            return topPlayer->getFigure(static_cast<Figure::FigureType>(type), number).getYPosition();
        else
            return bottomPlayer->getFigure(static_cast<Figure::FigureType>(type), number).getYPosition();
    }
    else if (topPlayer->getSide() == Figure::PlayerSide::White)
        return topPlayer->getFigure(static_cast<Figure::FigureType>(type), number).getYPosition();
    return bottomPlayer->getFigure(static_cast<Figure::FigureType>(type), number).getYPosition();
}

bool MatchManager::replaceFigure(int x, int y)
{
    for (auto cord : possibleTurnPlaceList)
        if (cord == std::make_pair(x, y)){
            auto cordSide = getFigureSideByCoordinate(x, y);
            if (cordSide == Figure::PlayerSide::OutOfDesk)
                return false;
            else if (cordSide != Figure::PlayerSide::None)
                getNotCurrentPlayer()->setNewPositionToFugure(cord, std::make_pair(INT_MAX, INT_MAX));
            currentPlayer->setNewPositionToFugure(cordByLastChooseFigure, cord);
            possibleTurnPlaceList.clear();
            cordByLastChooseFigure = std::make_pair(INT_MAX, INT_MAX);
            QMetaObject::invokeMethod(desk, "clearTurnLayer");
            return true;
        }
    return false;
}

void MatchManager::clickToDesk(int x, int y)
{
    qDebug() << x << y;
    // test click or not out of desk
    if (x < 0 || x >= 9 || y < 0 || y >= 10){
        QMetaObject::invokeMethod(desk, "clearTurnLayer");
        return;
    }

    // test possible move
    if (! replaceFigure(x, y))
        // find possible turn
        findPossibleTurn(x, y);
    else {
        // try test victory
        checkVictory();
        switchTurnSide();
    }
}

void MatchManager::switchTurnSide()
{
    currentPlayer = getNotCurrentPlayer();
}

Player* MatchManager::getNotCurrentPlayer()
{
    if (currentPlayer == topPlayer)
        return bottomPlayer;
    return topPlayer;
}

void MatchManager::checkVictory()
{
    Figure::PlayerSide loseSide(Figure::PlayerSide::None);
    if (! topPlayer->getFigure(Figure::FigureType::General).isOnDesk())
        loseSide = topPlayer->getSide();
    else if (! bottomPlayer->getFigure(Figure::FigureType::General).isOnDesk())
        loseSide = bottomPlayer->getSide();
    if (loseSide != Figure::PlayerSide::None){
        QString text;
        loseSide == Figure::PlayerSide::Red ? text = "White" : text = "Red";
        text += " WIN";
        QMetaObject::invokeMethod(desk, "setResultText", Q_ARG(QVariant, text));
    }
}

void MatchManager::draw()
{
    //
}

void MatchManager::startOnePlayerMatch()
{
    //
}

void MatchManager::startTwoPlayerMatch()
{
    //
}

void MatchManager::startLocalMatch()
{
    //
}
