#include "mainmanager.h"

MainManager::MainManager(QObject *parent) : QObject(parent)
{
    qmlRegisterType<Account>("Account", 1, 0, "Account");
    qmlRegisterType<Figure>("Figure", 1, 0, "Figure");
    //qmlRegisterType<Player>("Player", 1, 0, "Player");

    match = new MatchManager;

    view = new QQuickView();
    view->rootContext()->setContextProperty("Qt_main", this);
    view->rootContext()->setContextProperty("Qt_match", match);
    view->setSource(QUrl("qrc:/QML/main.qml"));
    view->resize(365, 400);
    view->show();

    QMetaObject::invokeMethod(view->rootObject(), "getDeskObject");
}

void MainManager::quit()
{
    QGuiApplication::quit();
}
