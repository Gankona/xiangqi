QT += gui
QT += qml
QT += quick

CONFIG += c++14
TARGET = "Xiangqi"

OTHER_FILES += \
    QML/*.qml

DISTFILES += \
    QML/main.qml \
    QML/Desk.qml

RESOURCES += \
    resources.qrc

SOURCES += \
    main.cpp \
    C++/logic.cpp \
    C++/matchmanager.cpp \
    C++/figure.cpp \
    C++/player.cpp \
    C++/settings.cpp \
    C++/mainmanager.cpp \
    C++/account.cpp

HEADERS += \
    C++/logic.h \
    C++/matchmanager.h \
    C++/figure.h \
    C++/player.h \
    C++/settings.h \
    C++/mainmanager.h \
    C++/account.h
