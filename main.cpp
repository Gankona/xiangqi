#include <QGuiApplication>
#include "C++/mainmanager.h"

int main(int argc, char** argv){
    QGuiApplication app(argc, argv);
    app.setWindowIcon(QIcon(":/figure/svg/soldierW.svg.png"));

    MainManager *manager = new MainManager;

    return app.exec();
}
