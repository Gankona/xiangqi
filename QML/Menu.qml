import QtQuick 2.5

Rectangle {
    id: menuPanel
    anchors.centerIn: parent
    color: color_background

    Column {
        spacing: menuPanel.height / 20
        anchors.centerIn: parent

        Rectangle {
            id: onePlayerRect
            color: "grey"//"#ffee00"
            radius: 10.0
            width: menuPanel.width - 20
            height: topPanel.height
            Text {
                anchors.centerIn: parent
                font.pointSize: 12
                text: "One Player"
            }
//            MouseArea {
//                anchors.fill: parent
//                onClicked: {
//                    hideAll();
//                    desk.visible = true;
//                    Qt_match.startOnePlayerMatch();
//                }
//            }
        }
        Rectangle {
            id: dualPlayerRect
            color: "#ffdd00"
            radius: 10.0
            width: menuPanel.width - 20
            height: topPanel.height
            Text {
                anchors.centerIn: parent
                font.pointSize: 12
                text: "Two Player"
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    hideAll();
                    desk.visible = true;
                    //twoPlayerMenu.visible = true;
                }
            }
        }
        Rectangle {
            id: localPlayerRect
            color: "grey"//"#ffcc00"
            radius: 10.0
            width: menuPanel.width - 20
            height: topPanel.height
            Text {
                anchors.centerIn: parent
                font.pointSize: 12
                text: "Local game"
            }
//            MouseArea {
//                anchors.fill: parent
//                onClicked: {
//                    hideAll();
//                    localMenu.visible = true;
//                }
//            }
        }
        Rectangle {
            id: settingsRect
            color: "grey"//"#ffbb00"
            radius: 10.0
            width: menuPanel.width - 20
            height: topPanel.height
            Text {
                anchors.centerIn: parent
                font.pointSize: 12
                text: "Settings"
            }
//            MouseArea {
//                anchors.fill: parent
//                onClicked: {
//                    hideAll();
//                    settings.visible = true;
//                }
//            }
        }
        Rectangle {
            id: exitRect
            color: "#ffaa00"
            radius: 10.0
            width: menuPanel.width - 20
            height: topPanel.height
            Text {
                anchors.centerIn: parent
                font.pointSize: 12
                text: "Exit"
            }
            MouseArea {
                anchors.fill: parent
                onClicked: Qt_main.quit()
            }
        }
    }
}
