import QtQuick 2.5
import QtQml 2.2
import "qrc:/QML"

Item {
    id: mainWidget
    anchors.fill: parent

    property string color_background: "#bf4c19"
    property int deskSize: 0

    function resizeEvent(height, width){
        deskSize = Math.min(height - topPanel.height, width);
    }

    function getDeskObject(){
        Qt_match.setDesk(desk);
    }

    function setPossibleTurn(current, enemyFigure, enemyPath, playerPath){
        desk.setPossibleTurn(current, enemyFigure, enemyPath, playerPath);
    }

    function hideAll(){
        account.visible = false;
        desk.visible = false;
        localMenu.visible = false;
        menu.visible = false;
        settings.visible = false;
        twoPlayerMenu.visible = false;
    }

    Rectangle {
        id: topPanel
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        height: parent.height / 13
        color: "navy"

        Rectangle {
            id: menuButton
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            width: menuButton.height
            color: parent.color
            Text {
                anchors.centerIn: parent
                text: "<"
                color: "white"
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.pointSize: 16
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    hideAll();
                    menu.visible = true;
                }
            }
        }

        Text {
            id: title
            anchors.top: parent.top
            anchors.left: menuButton.right
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            verticalAlignment: Text.AlignVCenter
            font.pointSize: 12//parent.height / 3
            text: "Title"
        }

        Text {
            id: accountText
            anchors.top: parent.top
            anchors.left: menuButton.right
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignRight
            font.pointSize: 10//parent.height / 3
            text: "Guest"
        }
    }

    Rectangle {
        id: appRect
        anchors.top: topPanel.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom

        Account {
            id: account
            anchors.fill: parent
            visible: false
        }

        Desk {
            id: desk
            anchors.fill: parent
            visible: false
        }

        LocalMenu {
            id: localMenu
            anchors.fill: parent
            visible: false
        }

        Menu {
            id: menu
            anchors.fill: parent
            visible: true
        }

        Settings {
            id: settings
            anchors.fill: parent
            visible: false
        }

        TwoPlayerMenu {
            id: twoPlayerMenu
            anchors.fill: parent
            visible: false
        }
    }

    onHeightChanged : resizeEvent(height, width)
    onWidthChanged  : resizeEvent(height, width);
}
